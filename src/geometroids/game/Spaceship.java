package geometroids.game;

import geometroids.main.core.Keys;
import geometroids.main.core.Main;
import geometroids.main.core.PhysicsObject;
import geometroids.main.util.Statistics;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

/** Bewegt und visualisiert das Raumschiff. */
@SuppressWarnings("serial")
public class Spaceship extends PhysicsObject
{
    /** Enth�lt alle Laser, die verschossen wurden. */
    private final ArrayList<Laser> lasers;

    /** Initialisiert das Raumschiff */
    public Spaceship()
    {
        init(Color.ORANGE,
             new Polygon(
                     new int[]
                     {
                         0, 50, 25
                     },
                     new int[]
                     {
                         50, 50, -13
                     }, 3),
             0,
             Main.WIDTH / 2 - 25,
             Main.HEIGHT * 0.9,
             50,
             50,
             8,
             0,
             0.1);

        lasers = new ArrayList<Laser>();
    }

    /**
     * Aktualisiert das Raumschiff und alle Laser.
     * @param asteroids Asteroiden mit denen die Laser kollidieren sollen
     */
    public void update(ArrayList<Asteroid> asteroids)
    {
        processInput();
        moveThroughWall();

        ArrayList<Laser> oldLasers = new ArrayList<Laser>(lasers);
        for(Laser laser : oldLasers)
        {
            laser.update(asteroids, this);
        }
    }

    /**
     * Zeichnet das Raumschiff und alle Laser.
     * @param graphics Graphics-Objekt, auf dem gezeichnet wird
     */
    @Override
    public void paint(Graphics2D graphics)
    {
        super.paint(graphics);
        for(Laser laser : lasers)
        {
            laser.paint(graphics);
        }
    }

    /** Verarbeitet die Tastatureingaben und bewegt das Raumschiff. */
    private void processInput()
    {
        if(Keys.getPressed(KeyEvent.VK_Q))
        {
            rotate(-1);
        }
        if(Keys.getPressed(KeyEvent.VK_E))
        {
            rotate(1);
        }
        if(Keys.getPressed(KeyEvent.VK_A))
        {
            translateX(-1);
        }
        if(Keys.getPressed(KeyEvent.VK_D))
        {
            translateX(1);
        }
        if(Keys.getReleased(KeyEvent.VK_W) && lasers.size() < 10)
        {
            lasers.add(new Laser(getRot(), getPosX(), getPosY()));
            Statistics.addToStatistic("Total Shots");
        }
    }

    /**
     * L�scht einen Laser, zum Beispiel wenn er getroffen hat.
     * @param laser Der Laser
     */
    public void destroyLaser(Laser laser)
    {
        lasers.remove(laser);
    }
}
