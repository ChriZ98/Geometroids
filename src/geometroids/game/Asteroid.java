package geometroids.game;

import geometroids.main.core.Main;
import geometroids.main.core.PhysicsObject;
import geometroids.main.util.Random;
import geometroids.main.util.Statistics;
import java.awt.Color;
import java.awt.Polygon;
import java.awt.Shape;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

/** Zeigt Ateroiden an und bewegt sie. */
@SuppressWarnings("serial")
public class Asteroid extends PhysicsObject
{
    /** Initialisiert den Asteroiden. */
    public Asteroid()
    {
        reset();
    }

    /**
     * L�sst den Asteroiden fallen und rotieren.
     * Au�erdem wird der Asteroid durch den Fensterrand, also zur anderen Seite bewegt.
     * Falls der Asteroid unten ankommt wird er nach oben gesetzt.
     */
    public void update()
    {
        rotate(1);
        translateX(1);
        translateY(1);
        moveThroughWall();
        if(isOutOfBoundsBottom())
        {
            Statistics.addToStatistic("Passed Asteroids");
            reset();
        }
    }

    /**
     * Setzt den Asteroiden zur�ck.
     * Dabei werden zuf�llige Formen, Farben, Gr��en, Geschwindigkeiten und Positionen verwendet.
     */
    public void reset()
    {
        double size = Random.asInt(50, 100);
        init(getRandomColor(),
             getRandomShape(size),
             0,
             Random.asInt(0, Main.WIDTH - size),
             0,
             size,
             size,
             Random.asDouble(-1.5, 1.5),
             Random.asDouble(2, 5),
             Random.asDouble(0.05, 0.1));
    }

    /**
     * W�hlt eine zuf�llige Form aus.
     * Diese wird automatisch an die Gr��e angepasst.
     * M�gliche Formen sind: Quadrat, Kreis, Balken, Dreieck.
     * @param size Gr��e der Form, Breite und H�he
     * @return Zuf�llige Form
     */
    private Shape getRandomShape(double size)
    {
        int rand = Random.asInt(0, 4);
        if(rand == 0)
        {
            return new Rectangle2D.Double(0, 0, size, size);
        }
        else if(rand == 1)
        {
            return new Ellipse2D.Double(0, 0, size, size);
        }
        else if(rand == 2)
        {
            return new Rectangle2D.Double(0, size / 2 - 5, size, 10);
        }
        else
        {
            double halfSize = size / 2.0;
            double height = size * 0.86602540378443864676372317075294;
            double offset = ((3.0 * halfSize) - height) / 3.0;
            height += offset;
            return new Polygon(
                    new int[]
                    {
                        0, (int)size, (int)halfSize
                    },
                    new int[]
                    {
                        (int)offset, (int)offset, (int)height
                    }, 3);
        }
    }

    /**
     * W�hlt eine zuf�llige Farbe aus.
     * @return Zuf�llige Farbe
     */
    private Color getRandomColor()
    {
        return new Color(Random.asInt(20, 250), Random.asInt(20, 250), Random.asInt(20, 250));
    }
}
