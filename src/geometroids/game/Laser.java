package geometroids.game;

import geometroids.main.core.PhysicsObject;
import geometroids.main.util.Statistics;
import java.awt.Color;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

/** Wird vom Raumschiff verschossen und fliegt bis zum Bildrand oder Asteroiden. */
@SuppressWarnings("serial")
public class Laser extends PhysicsObject
{
    /**
     * Initialisiert den Laser mit Rotation und Position des Raumschiffs.
     * Teilt mit sin() und cos() die Geschwindigkeit auf die Richtung auf um korrekt zu fliegen.
     * @param rot  Rotation des Raumschiffs
     * @param posX Horizontale Position des Raumschiffs
     * @param posY Vertikale Position des Raumschiffs
     */
    public Laser(double rot, double posX, double posY)
    {
        init(Color.RED,
             new Rectangle2D.Double(23, 0, 4, 50),
             rot,
             posX,
             posY,
             50,
             4,
             -6 * Math.sin(rot),
             6 * Math.cos(rot),
             0);
    }

    /**
     * Bewegt den Laser und pr�ft Kollisionen.
     * @param asteroids Asteroiden, die kollidieren k�nnten
     * @param spaceship Raumschiff, zu dem der Laser geh�rt
     */
    public void update(ArrayList<Asteroid> asteroids, Spaceship spaceship)
    {
        translateX(-1);
        translateY(-1);
        moveThroughWall();
        checkHit(asteroids, spaceship);
        checkOutOfBounds(spaceship);
    }

    /**
     * Pr�ft ob einer der Asteroiden getroffen wurde.
     * Falls ja werden der Asteroid und der Laser zerst�rt.
     * @param asteroids Asteroiden, die kollidieren k�nnten
     * @param spaceship Raumschiff, zu dem der Laser geh�rt
     */
    private void checkHit(ArrayList<Asteroid> asteroids, Spaceship spaceship)
    {
        for(Asteroid asteroid : asteroids)
        {
            if(getHitBox().intersects(asteroid.getHitBox()))
            {
                Statistics.addToStatistic("Destroyed Asteroids");
                asteroid.reset();
                spaceship.destroyLaser(this);
            }
        }
    }

    /**
     * Pr�ft ob der Laser au�erhalb des Fensters ist und l�scht ihn gegebenenfalls.
     * @param spaceship Raumschiff, zu dem der Laser geh�rt
     */
    private void checkOutOfBounds(Spaceship spaceship)
    {
        if(isOutOfBoundsTop() || isOutOfBoundsBottom())
        {
            Statistics.addToStatistic("Missed Shots");
            spaceship.destroyLaser(this);
        }
    }
}
