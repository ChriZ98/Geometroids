package geometroids.main.util;

import java.util.HashMap;
import java.util.Map.Entry;

/** Sammelt Daten und kann sie sp�ter ausgeben. */
public class Statistics
{
    /** Enth�lt alle Z�hler mit Namen und Wert. */
    private static final HashMap<String, Double> statistics = new HashMap<>();

    /**
     * Erh�ht einen Z�hler um 1.
     * Versucht sich einen bestehenden Z�hler zu holen und zu erh�hen.
     * Falls dieser nicht vorhanden ist wird ein neuer Z�hler eingef�gt.
     * @param name Name des Z�hlers
     */
    public static void addToStatistic(String name)
    {
        Double value = statistics.get(name);
        if(value == null)
        {
            statistics.put(name, 1.0);
        }
        else
        {
            value++;
            statistics.put(name, value);
        }
    }

    /**
     * Baut aus allen Z�hlern einen Text auf, der ausgegeben werden kann.
     * @return Den Text
     */
    public static String toText()
    {
        StringBuilder buffer = new StringBuilder();
        for(Entry<String, Double> entry : statistics.entrySet())
        {
            buffer.append(entry.toString());
            buffer.append("; ");
        }
        return buffer.toString();
    }
}
