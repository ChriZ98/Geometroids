package geometroids.main.util;

/** Generiert Zufallszahlen. */
public class Random
{
    /**
     * Generiert eine Zufallszahl ohne Nachkommastellen.
     * @param from Anfang des gewŁnschten Bereichs
     * @param to   Ende des gewŁnschten Bereichs
     * @return Zufallszahl aus dem Bereich
     */
    public static int asInt(double from, double to)
    {
        return (int)asDouble(from, to);
    }

    /**
     * Generiert eine Zufallszahl mit Nachkommastellen.
     * @param from Anfang des gewŁnschten Bereichs
     * @param to   Ende des gewŁnschten Bereichs
     * @return Zufallszahl aus dem Bereich
     */
    public static double asDouble(double from, double to)
    {
        return (Math.random() * (to - from)) + from;
    }
}
