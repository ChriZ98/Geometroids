package geometroids.main.core;

import java.awt.EventQueue;
import javax.swing.JFrame;

/**
 * Beinhaltet nur die main-Methode.
 * Startet das gesamte Programm.
 * Initialisiert das Fenster.
 */
public class Main
{
    /** Gibt die Bilder pro Sekunde f�r die Anwendung an. */
    public static final int FPS = 40;
    /** Breite des Anwendungsfensters */
    public static final int WIDTH = 800;
    /** H�he des Anwendungsfensters */
    public static final int HEIGHT = 600;

    /**
     * Wird vom Betriebssystem automatisch beim Starten der Anwendung ausgef�hrt.
     * Erzeugt ein Fenster mit Titel und f�gt das Panel hinzu.
     * Bestimmt, dass die Anwendung beim Klicken auf X beendet wird.
     * Verhindert nachtr�gliche Gr��en�nderungen am Fenster.
     * Packt alle Komponenten auf das Fenster.
     * Zeigt das Fenster an und positioniert das Fenster in der Mitte des Bildschirms.
     *
     * ===== Informationen zu EventQueue.invokeLater =====
     * Certain swing methods needs to be invoked in Swing thread. The Swing methods show(),
     * setVisible(), and pack() will create the associated peer for the frame. With the creation
     * of the peer, the system creates the event dispatch thread. This makes things problematic
     * because the event dispatch thread could be notifying listeners while pack and validate are
     * still processing. This situation could result in two threads going through the Swing
     * component-based GUI -- it's a serious flaw that could result in deadlocks or other related
     * threading issues. A pack call causes components to be realized. As they are being realized
     * (that is, not necessarily visible), they could trigger listener notification on the event
     * dispatch thread.
     *
     * @param args Es k�nnen Kommandozeilen-Parameter �bergeben werden (nicht verwendet)
     */
    public static void main(String[] args)
    {
        EventQueue.invokeLater(() ->
        {
            JFrame frame = new JFrame("Geometroids");
            frame.setContentPane(new Control());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setResizable(false);
            frame.pack();
            frame.setVisible(true);
            frame.setLocationRelativeTo(null);
        });
    }
}
