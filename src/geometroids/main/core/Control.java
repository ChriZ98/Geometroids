package geometroids.main.core;

import geometroids.game.Asteroid;
import geometroids.game.Spaceship;
import geometroids.main.util.Statistics;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Repr�sentiert den Fensterinhalt.
 * Bekommt Tastatureingaben vom Betriebssystem und �bergibt diese an die Keyboard-Klasse.
 * Verwaltet alle Objekte, die angezeigt werden sollen und aktualisiert diese.
 */
@SuppressWarnings("serial")
public class Control extends JPanel implements ActionListener
{
    /** Das Raumschiff, welches gesteuert werden kann. */
    private final Spaceship spaceship;
    /** Enth�lt alle Asteroiden, die herumfliegen sollen. */
    private final ArrayList<Asteroid> asteroids;

    /**
     * Initialisiert den gesamten Fensterinhalt und startet den Timer f�r die Aktualisierung aller Komponenten.
     * Setzt das Layout auf null, damit von allen Komponenten die Position exakt definiert werden kann.
     * Erh�lt den Fokus, damit Tastatureingaben registriert werden k�nnen.
     * Leitet die Eingaben an die Keys-Klasse weiter und setzt die Gr��e des inneren Fensters.
     * Erzeugt ein Raumschiff und 10 Asteroiden.
     * Erzeugt und startet einen Timer, der je nach eingestellter FPS-Rate �fter oder seltener alle Objekte aktualisiert.
     */
    public Control()
    {
        this.setLayout(null);
        this.setFocusable(true);
        this.requestFocus();
        this.addKeyListener(new Keys());
        this.setPreferredSize(new Dimension(Main.WIDTH, Main.HEIGHT));

        spaceship = new Spaceship();

        asteroids = new ArrayList<>();
        for(int i = 0; i < 10; i++)
        {
            Asteroid asteroid = new Asteroid();
            asteroids.add(asteroid);
        }

        Timer tickTimer = new Timer(1000 / Main.FPS, this);
        tickTimer.start();
    }

    /**
     * Wird automatisch in bestimmten Intervallen vom oben gestarteten Timer aufgerufen.
     * Hier werden alle Elemente aktualisiert, also bewegt, gedreht, etc.
     * Danach wird der gesamte Bildschirm neu gezeichnet um alle �nderungen mitzubekommen.
     * @param ae Event-ID, Normalerweise k�nnen mehrere ActionEvents hier ankommen, aber es wurde nur der Timer dazu aufgefordert
     */
    @Override
    public void actionPerformed(ActionEvent ae)
    {
        for(Asteroid asteroid : asteroids)
        {
            asteroid.update();
        }
        spaceship.update(asteroids);
        repaint();
    }

    /**
     * Wird beim repaint() (und teilweise automatisch) aufgerufen.
     * @param grphcs Graphics-Objekt auf dem gezeichnet wird
     */
    @Override
    public void paintComponent(Graphics grphcs)
    {
        super.paintComponent(grphcs);
        paint((Graphics2D)grphcs);
    }

    /**
     * Setzt die Grafikeinstellungen und zeichnet alle Elemnte.
     * @param graphics Graphics2D-Objekt auf dem gezeichnet wird
     */
    private void paint(Graphics2D graphics)
    {
        graphics.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        graphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        for(Asteroid asteroid : asteroids)
        {
            asteroid.paint(graphics);
        }
        spaceship.paint(graphics);

        graphics.setColor(new Color(0, 0, 0, 0.4f));
        graphics.fillRect(0, 0, Main.WIDTH, 30);

        graphics.setColor(Color.BLACK);
        graphics.setFont(new Font("Calibri", Font.BOLD, 18));

        graphics.drawString(Statistics.toText(), 5, 20);
    }
}
