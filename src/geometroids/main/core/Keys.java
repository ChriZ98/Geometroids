package geometroids.main.core;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Arrays;

/** Diese Klasse verwaltet alle Tastatureingaben. */
public class Keys extends KeyAdapter
{
    /** Beinhaltet Wahrheitswerte f�r alle Tasten, also ob diese gedr�ckt wurden. */
    private static final boolean[] pressed = new boolean[KeyEvent.KEY_LAST];
    /** Beinhaltet Wahrheitswerte f�r alle Tasten, also ob diese losgelassen wurden. */
    private static final boolean[] released = new boolean[KeyEvent.KEY_LAST];

    /**
     * Liefert Status einer Taste
     * @param id Tasten-ID
     * @return Wurde die Taste gedr�ckt?
     */
    public static boolean getPressed(int id)
    {
        return pressed[id];
    }

    /**
     * Liefert Status einer Taste
     * @param id Tasten-ID
     * @return Wurde die Taste losgelassen?
     */
    public static boolean getReleased(int id)
    {
        boolean isReleased = released[id];
        Arrays.fill(released, false);
        return isReleased;
    }

    /**
     * Diese Methode wird automatisch von Java aufgerufen, wenn eine Taste gedr�ckt wurde (siehe Control).
     * @param ke Tasten-ID und mehr
     */
    @Override
    public void keyPressed(KeyEvent ke)
    {
        pressed[ke.getKeyCode()] = true;
    }

    /**
     * Diese Methode wird automatisch von Java aufgerufen, wenn eine Taste losgelassen wurde (siehe Control).
     * @param ke Tasten-ID und mehr
     */
    @Override
    public void keyReleased(KeyEvent ke)
    {
        pressed[ke.getKeyCode()] = false;
        released[ke.getKeyCode()] = true;
    }
}
