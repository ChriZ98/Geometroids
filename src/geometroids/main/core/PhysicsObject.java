package geometroids.main.core;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;

/** Ein Objekt mit Farbe, Form, Position und Gr��e, das bewegt werden kann. */
public class PhysicsObject
{
    /** Farbe des Objekts */
    private Color color;
    /** Form des Objekts */
    private Shape shape;

    /** Drehung des Objekts */
    private double rot;
    /** Horizontale Position des Objekts */
    private double posX;
    /** Vertikale Position des Objekts */
    private double posY;
    /** Horizontale Gr��e des Objekts */
    private double sizeX;
    /** Vertikale Gr��e des Objekts */
    private double sizeY;
    /** Horizontale Geschwindigkeit des Objekts */
    private double speedX;
    /** Vertikale Geschwindigkeit des Objekts */
    private double speedY;
    /** Rotationsgeschwindigkeit des Objekts */
    private double speedRot;

    /**
     * Initialisiert ein Objekt.
     * Dies muss in einer Methode geschehen, da teilweise vorangehende Berechnungen notwendig sind.
     * @param color    Farbe des Objekts
     * @param shape    Form des Objekts
     * @param rot      Drehung des Objekts
     * @param posX     Horizontale Position des Objekts
     * @param posY     Vertikale Position des Objekts
     * @param sizeX    Horizontale Gr��e des Objekts
     * @param sizeY    Vertikale Gr��e des Objekts
     * @param speedX   Horizontale Geschwindigkeit des Objekts
     * @param speedY   Vertikale Geschwindigkeit des Objekts
     * @param speedRot Rotationsgeschwindigkeit des Objekts
     */
    protected void init(Color color, Shape shape, double rot, double posX, double posY, double sizeX, double sizeY, double speedX, double speedY, double speedRot)
    {
        this.color = color;
        this.shape = shape;

        this.rot = rot;
        this.posX = posX;
        this.posY = posY;
        this.sizeX = sizeX;
        this.sizeY = sizeY;
        this.speedX = speedX;
        this.speedY = speedY;
        this.speedRot = speedRot;
    }

    /**
     * Zeichnet das Objekt.
     * @param graphics Graphics-Objekt, auf dem gezeichnet wird
     */
    public void paint(Graphics2D graphics)
    {
        AffineTransform oldTransform = graphics.getTransform();

        AffineTransform transform = new AffineTransform();
        transform.translate(posX, posY);
        transform.rotate(rot, sizeX / 2, sizeY / 2);
        graphics.transform(transform);

        graphics.setColor(color);
        graphics.fill(shape);

        graphics.setTransform(oldTransform);
    }

    /** Bewegt ein Objekt durch den Fensterrand, also zur anderen Seite. */
    protected void moveThroughWall()
    {
        if(isOutOfBoundsLeft())
        {
            posX = Main.WIDTH - (sizeX / 2);
        }
        else if(isOutOfBoundsRight())
        {
            posX = -sizeY / 2;
        }
    }

    /**
     * Pr�ft die Position des Objekts im Bezug auf den Linken Fensterrand.
     * @return Ist das Objekt hinter dem linken Fensterrand?
     */
    private boolean isOutOfBoundsLeft()
    {
        return sizeX / 2 + posX <= 0;
    }

    /**
     * Pr�ft die Position des Objekts im Bezug auf den rechten Fensterrand.
     * @return Ist das Objekt hinter dem rechten Fensterrand?
     */
    private boolean isOutOfBoundsRight()
    {
        return sizeX / 2 + posX >= Main.WIDTH;
    }

    /**
     * Pr�ft die Position des Objekts im Bezug auf den oberen Fensterrand.
     * @return Ist das Objekt hinter dem oberen Fensterrand?
     */
    protected boolean isOutOfBoundsTop()
    {
        return sizeY / 2 + posY <= 0;
    }

    /**
     * Pr�ft die Position des Objekts im Bezug auf den unteren Fensterrand.
     * @return Ist das Objekt hinter dem unteren Fensterrand?
     */
    protected boolean isOutOfBoundsBottom()
    {
        return sizeY / 2 + posY >= Main.HEIGHT;
    }

    /**
     * Dreht das Objekt.
     * @param factor Faktor der Drehung
     */
    protected void rotate(int factor)
    {
        rot += factor * speedRot;
    }

    /**
     * bewegt das Objekt horizontal.
     * @param factor Faktor der Bewegung
     */
    protected void translateX(int factor)
    {
        posX += factor * speedX;
    }

    /**
     * Bewegt das Objekt vertikal.
     * @param factor Faktor der Bewegung
     */
    protected void translateY(int factor)
    {
        posY += factor * speedY;
    }

    /**
     * Errechnet die Hitbox des Objekts.
     * @return Hitbox
     */
    public Rectangle2D getHitBox()
    {
        return new Rectangle2D.Double(posX, posY, sizeX, sizeY);
    }

    /**
     * Gibt Rotation.
     * @return Rotation
     */
    protected double getRot()
    {
        return rot;
    }

    /**
     * Gibt horizontale Position.
     * @return Position
     */
    protected double getPosX()
    {
        return posX;
    }

    /**
     * Gibt vertikale Position.
     * @return Position
     */
    protected double getPosY()
    {
        return posY;
    }

    /**
     * Gibt horizontale Gr��e.
     * @return Gr��e
     */
    protected double getSizeX()
    {
        return sizeX;
    }

    /**
     * Gibt vertikale Gr��e.
     * @return Gr��e
     */
    protected double getSizeY()
    {
        return sizeY;
    }
}
